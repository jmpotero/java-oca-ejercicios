package main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


public class Ejercicios {

	public char[] ejercicio1() 
	{	
		char[] listaLetras = new char[26];
		
		for(int i = 0; i < 26; i++)
		{
			listaLetras[i] = (char) (i+97);
		}
		
		return listaLetras;
	}

	public char[] ejercicio2(char[] letras) {
		for(int i = 0; i < 26; i++)
		{
			if((int) letras[i] % 2 != 0)
				letras[i] = Character.toUpperCase((Character) letras[i]);
		}
		
		return letras;
	}

	public String ejercicio3(char[] listaInicial) {
		return listaInicial.toString();
	}
	
	public String ejercicio3(char[] listaInicial, int salto) {
		StringBuilder listaConSaltos = new StringBuilder();
		
		for(int i = 0; i < listaInicial.length; i = i + salto + 1)
		{
			listaConSaltos.append(listaInicial[i]);
		}
		
		return listaConSaltos.toString();
	}
	
	public String ejercicio3(String cadena) {
		StringBuilder cadenaRetorno = new StringBuilder(cadena);
		cadenaRetorno.reverse();
		
		return cadenaRetorno.toString();
	}
	
	public boolean ejercicio6(String cadena)
	{
		StringBuilder tmp = new StringBuilder(cadena);
		StringBuilder cadenaInversa = tmp.reverse();
		
		if (cadena.equals(cadenaInversa.toString()))
			return true ;
		else
			return false;
	}
	
	public String ejercicio7(String[] listaCadenas)
	{
		StringBuilder cadena = new StringBuilder(listaCadenas[0]);
		
		for (int i = 1; i < listaCadenas.length; i++)
		{
			StringBuilder aux = new StringBuilder();
			aux.append(Character.toUpperCase(listaCadenas[i].charAt(0)));
			aux.append(listaCadenas[i].substring(1));
			cadena.append(aux);
		}
		
		return cadena.toString();
	}

	public String[] ejercicio8(String cadenaIngresada, String[] listaCadenasIngresada) 
	{
		StringBuilder cadena = new StringBuilder(cadenaIngresada);
		StringBuilder cadenaInversa = new StringBuilder(cadenaIngresada);
		cadenaInversa.reverse();
		List<String> listaRetorno = new ArrayList<String>();
		
		for(String unaCadena: listaCadenasIngresada)
		{
			if(cadena.indexOf(unaCadena) != -1 || cadenaInversa.indexOf(unaCadena) != -1)
				listaRetorno.add(unaCadena);
		}
		
		String[] listaRetornoArray = new String[listaRetorno.size()];
		
		for(int i = 0; i < listaRetorno.size(); i++)
		{
			listaRetornoArray[i] = listaRetorno.get(i);
		}
		
		return listaRetornoArray;
	}
	
	public Calendar ejercicio9(int numeroIngresado)
	{
		Double segundos1900 = 1900d * 365d * 24d * 60d * 60d;
		Double segundosTotales = segundos1900 + numeroIngresado;
		Integer year = (int) (segundosTotales / 60 / 60 / 24 / 365);
		Integer month = numeroIngresado / 60 / 60 / 24 / 30;
		Double dayD = numeroIngresado / 60 / 60 / 24d;
		BigDecimal aux = BigDecimal.valueOf(dayD);
		aux = aux.setScale(0, BigDecimal.ROUND_CEILING);
		Integer day = aux.intValue();
		
		Calendar fechaRetorno = new GregorianCalendar(year, month, day);
		
		return fechaRetorno;
	}
}
