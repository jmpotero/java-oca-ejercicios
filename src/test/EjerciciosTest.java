package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;

import main.Ejercicios;

import org.junit.Before;
import org.junit.Test;

public class EjerciciosTest {
	private Ejercicios ejercicios;
	private char[] listaInicial;

	@Before
	public void setup()
	{
		ejercicios = new Ejercicios();
		listaInicial = ejercicios.ejercicio1();
	}
	
	@Test
	public void ejercicio1_test() {
		char[] listaEsperada = new char[26];
		for(int i = 0; i < 26; i++)
		{
			listaEsperada[i] = (char) (i+97);
		}
		char[] listaObtenida = ejercicios.ejercicio1();
		
		assertArrayEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio2_test() {
		char[] listaEsperada = listaInicial;
		for(int i = 0; i < 26; i++)
		{
			if((int) listaEsperada[i] % 2 != 0)
				listaEsperada[i] = Character.toUpperCase((Character) listaEsperada[i]);
		}
		char[] listaObtenida = ejercicios.ejercicio2(listaInicial);
		
		assertArrayEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio3_test(){
		String listaEsperada = listaInicial.toString();
		
		String listaObtenida = ejercicios.ejercicio3(listaInicial);
		
		assertEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio4_test(){
		String listaEsperada = "aceg";
		char[] listaLetras = {'a','b','c','d','e','f','g'};
		String listaObtenida = ejercicios.ejercicio3(listaLetras, 1);
		
		assertEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio5_test()
	{
		String listaEsperada = "gfedcba";
		String cadena = "abcdefg";
		String listaObtenida = ejercicios.ejercicio3(cadena);
		
		assertEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio6_test()
	{
		String cadena = "ese";
		assertEquals(true, ejercicios.ejercicio6(cadena));
	}
	
	@Test
	public void ejercicio7_test()
	{
		String[] listaCadenas = {"uno", "dos", "tres", "cuatro"};
		String listaEsperada = "unoDosTresCuatro";
		
		String listaObtenida = ejercicios.ejercicio7(listaCadenas);
		
		assertEquals(listaEsperada, listaObtenida);
	}
	
	@Test
	public void ejercicio8_test()
	{
		String cadenaIngresada = "lalistarapodoc";
		String[] listaCadenasIngresada = {"lista", "para", "codo", "hola", "java"};
		String[] listaCadenasEsperada = {"lista", "para", "codo"};
		
		String[] listaCadenasObtenida = ejercicios.ejercicio8(cadenaIngresada, listaCadenasIngresada);
		
		assertArrayEquals(listaCadenasEsperada, listaCadenasObtenida);
	}
	
	@Test
	public void ejercicio9_test()
	{
		Integer numeroIngresado = 200_000;
		Calendar fechaEsperada = new GregorianCalendar(1900, Calendar.JANUARY, 3);
		
		Calendar fechaObtenida = ejercicios.ejercicio9(numeroIngresado);
		
		assertEquals(fechaEsperada, fechaObtenida);
	}
}
